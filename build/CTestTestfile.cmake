# CMake generated Testfile for 
# Source directory: /home/vamshi/my_ws/src
# Build directory: /home/vamshi/my_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(turtlebot3_simulations/turtlebot3_msgs)
subdirs(turtlebot3_simulations/turtlebot3_simulations)
subdirs(my_turtlebot)
subdirs(turtlebot3_simulations/turtlebot3_fake)
subdirs(turtlebot3_simulations/turtlebot3_gazebo)
