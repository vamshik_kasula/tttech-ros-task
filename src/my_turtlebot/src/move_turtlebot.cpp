/**
 * \class move
 * \brief Class monitoring odom, updated goal and moving to the goal by avoiding obstacles.
 * \author Vamshikrishna Kasula.
 * \date 28.04.2019.
 * \ingroup my_ws
 */


#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <boost/math/quaternion.hpp>
#include <tf/tf.h>
#include <math.h>

#define Rad_Deg (180.0 / M_PI)
class Move
{
    public:
        
        /**
         * \brief Constructor
         * Initializes Subscribers, Publishers and class attributes
         */
        Move()
        {   
            //Initializing Subscribers
            sub_goal = nh.subscribe("/move_base_simple/goal", 1, &Move::goal_CallBack, this); 
            sub_odom = nh.subscribe("/odom", 1, &Move::odom_CallBack, this);
            sub_lds = nh.subscribe("/scan", 1, &Move::LDS_CallBack, this);

            //Initializing Publisher
            pub_twist = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
            
            /*Initializing initial pose of the goal*/
            goal.pose.position.x = 0;
            goal.pose.position.y = 0;
            goal.pose.orientation.w = 1;
            goal.pose.orientation.x = 0;
            goal.pose.orientation.y = 0;
            goal.pose.orientation.z = 0;
            
        }    
        
        /**
         * \brief Callback function for Selected Goal from Rviz
         * \param &msg : Received 'Goal' information
         */
        void goal_CallBack(const geometry_msgs::PoseStamped &msg)
        {
             goal.header = msg.header;
             goal.pose = msg.pose;
             ROS_INFO_STREAM("Goal : \n" <<goal); 
        }
        
        
        /**
         * \brief Callback function for odometry
         * \param &msg : Received odom information
         * Relative orientaion of the the Goal with respect to he robot is determined and \attribute ang_offset  is updated.
         */
        void odom_CallBack(const nav_msgs::Odometry &msg)
        {
            double roll, pitch, yaw,rob_ang, goal_ang, pos_x, pos_y;  
            
            pos_x = msg.pose.pose.position.x;
            pos_y = msg.pose.pose.position.y;
            
            //std::cout<<"Odom : \n" <<msg.pose;
            //std::cout<<"\nx-direction :" <<pos_x-goal.pose.position.x <<"\ty-direction :" <<pos_y-goal.pose.position.y;
            
            //Calculating relative orientation of the Goal with respective to the robots present orientation
            if(pos_x-goal.pose.position.x == 0)
                if (pos_x-goal.pose.position.y == 0)
                    goal_ang = 90;
                else
                    goal_ang = 0;
            else
                goal_ang = atan2((pos_y-goal.pose.position.y),(pos_x-goal.pose.position.x))*Rad_Deg +180;
            
            goal_dist = sqrt(pow(pos_x - goal.pose.position.x, 2) + pow(pos_y - goal.pose.position.y, 2));     //Displacement from current position to Goal
            
            
            //Converting from quaternion to rpy
            tf::Quaternion q(
            msg.pose.pose.orientation.x,
            msg.pose.pose.orientation.y,
            msg.pose.pose.orientation.z,
            msg.pose.pose.orientation.w);
            
            tf::Matrix3x3 m(q);
            m.getRPY(roll, pitch, yaw);
            
            rob_ang = yaw*Rad_Deg;          //Radians to degrees
            
            if(rob_ang < 0)
                rob_ang +=360;
            
            ang_offset = goal_ang-rob_ang;  //offset from present orientaion of the robot

            //std::cout<<"\nroll : " <<roll*Rad_Deg <<"\tpitch* : " <<pitch*Rad_Deg <<"\tyaw : " <<rob_ang <<"\tGoal ang : " <<goal_ang <<"\tang_off: " <<ang_offset;
            
            
        }
        

        /**
         * \brief Callback function for Laser Distance Sensor
         * \param &msg : Received LaserScan information
         * Checks for obstacle, if there is no obstacle found, \method reach_goal() is called
         */        
        void LDS_CallBack(const sensor_msgs::LaserScan &msg)
        {
            
            //std::cout<<"\nLDS Size : " <<msg.ranges.size();                                           
            //size of msg.range is 360 i.e., a reading per every degree
            //When the distance between obstacle and robot is less than 150mm (300mm from sensor), robot should change its direction
            //To detect an obstacle in the robots path, LDS data at 0 degrees (front), 30 and -30 (330) degrees need to be checked.
            //std::cout<<"\nLDS at 330 : " <<msg.ranges[330] <<"\tLDS at 0 : " <<msg.ranges[0] <<"\tLDS at 30 : " <<msg.ranges[30]; 
            if (msg.ranges[0] <= 0.45 || msg.ranges[30] <= 0.52)
                move_turtle(0, 0.26);
            else if( msg.ranges[330] <= 0.52)
                move_turtle(0, -0.26);
            else 
                reach_goal();
        }
        

        /**
         * \brief method to move the robot to destination
         * If the goal is not reached linear velcity and angular velocities are adjusted until goal is reached
         */        
        
        void reach_goal()
        {
            double lin_vel, ang_vel;

            if (fabs(goal_dist) < 0.1)
                lin_vel = 0;
            else
                lin_vel = 0.2;
            
            if (fabs(ang_offset) < 2)
                ang_vel = 0;
            else if ((ang_offset > 0 && ang_offset < 180)||(ang_offset < -180 && ang_offset >-360))
                ang_vel = 0.3;
            else
                ang_vel = -0.3;
            
            move_turtle(lin_vel, ang_vel);
            
        }
        
        
        /**
         * \brief method to update robots linear and angular velocity accordingly
         * 
         */        
        void move_turtle(double lin_vel, double ang_vel)
        {
            geometry_msgs::Twist vel_turtle;
            
            vel_turtle.linear.x = lin_vel;
            vel_turtle.angular.z = ang_vel;
            pub_twist.publish(vel_turtle);
        }
        
        
    
    
    private:    
        ros::NodeHandle nh;                                   // ROS nodehandle
        ros::Subscriber sub_goal, sub_odom, sub_lds;          // subscriber objects
        ros::Publisher pub_twist;                             // Publicher objects
        
        geometry_msgs::PoseStamped goal;                      // Attribute to save the present Goal selected
        double ang_offset, goal_dist;                         // Attributes to calculate goal pose

};


int main(int argc, char* argv[])
{
    ros::init(argc, argv, "move_turtlebot");                  // Initializing the node 
    Move Move_turtle;                                         // Class object

    ros::spin();

    return 0;
}
